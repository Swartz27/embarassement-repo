#include "stdafx.h"
#include "pch_script.h"
#include "bear.h"

using namespace luabind;

#pragma optimize("s",on)
void CAI_Bear::script_register(lua_State *L)
{
	module(L)
	[
		class_<CAI_Bear,CGameObject>("CAI_Bear")
			.def(constructor<>())
	];
}
