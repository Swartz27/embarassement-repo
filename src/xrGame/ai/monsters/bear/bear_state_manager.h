#pragma once
#include "../monster_state_manager.h"

class CAI_Bear;

class CStateManagerBear : public CMonsterStateManager<CAI_Bear> {
	typedef CMonsterStateManager<CAI_Bear> inherited;

public:

					CStateManagerBear	(CAI_Bear *monster); 

	virtual void	execute				();
	virtual void	remove_links		(CObject* object) { inherited::remove_links(object);}
};
